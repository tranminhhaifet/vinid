import sys
import numpy as np
from scipy.sparse import save_npz, load_npz
import pandas as pd
from sklearn.svm import LinearSVC
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier
from sklearn.metrics import precision_recall_fscore_support
import lightgbm as lgb
import xgboost as xgb
import matplotlib.pyplot as plt

from dataset import read_file, load_dataset

def model(X_train, y_train, type_model='svm'):
    """
    implementing model for classification
    """
    model = None
    if type_model == 'lg':
        model = LogisticRegression(C=0.1)
        model.fit(X_train, y_train)
    elif type_model == 'svm':
        model = LinearSVC(C=0.01, dual=False)
        model.fit(X_train, y_train)
    elif type_model == 'rf':
        model = RandomForestClassifier(n_estimators=100, n_jobs=-1, verbose=1)
        model.fit(X_train, y_train)
    elif type_model == 'gbc':
        model = GradientBoostingClassifier()
        model.fit(X_train, y_train)
    elif type_model == 'lgb':
        model = lgb.LGBMClassifier(n_estimators=300, learning_rate=0.05).fit(X_train, y_train)
    elif type_model == 'xgb':
        model = xgb.XGBClassifier(n_estimators=300, learning_rate=0.1, silent=False, n_jobs=4).fit(X_train, y_train)

    return model 

def draw_feature_importance(num_items, model, top=20):
    name_features = dict()
    for i in range(2*num_items):
        if i < num_items:
            name_features[i] = 'price of item %d' %i
        else:
            name_features[i] = 'sales quanity of item %d' %(i-num_items)
    i += 1
    name_features[i] = 'number of unique items'
    i += 1
    name_features[i] = 'number of active days'
    name_features[i+1] = 'mean of period'
    name_features[i+2] = 'std of period'
    name_features[i+3] = 'mean of salesquantity per day '
    name_features[i+4] = 'stardard of salesquanity per day'
    name_features[i+5] = 'num_days since the last purchase'
    name_features[i+6] = 'mean price per day'
    name_features[i+7] = 'standard price per day'
    print(len(list(name_features.keys())))

    importances = model.feature_importances_
    indices = np.argsort(importances)[::-1]
    ylabels = list()
    for f in range(top):
        ylabels.append(name_features[indices[f]])
    #     print('Feature %s (%f)' %(name_features[indices[f]], importances[indices[f]]))
    
    y_pos =  np.arange(len(ylabels))
    performance = importances[indices[:top]]
    #error = np.random.rand(len(ylabels))

    fig, ax = plt.subplots()
    ax.barh(y_pos, performance, align='center', color='blue')
    ax.set_yticks(y_pos)
    ax.set_yticklabels(ylabels)
    ax.set_xlabel('Importance')
    ax.set_title('Feature importances')
    plt.show()


if __name__ == '__main__':
    model_name = sys.argv[1]
    num_users, num_items, df = read_file('MLTrack_DataSet.csv')

    # normalization salesquantity and price attributes
    df['salesquantity'] = (df['salesquantity'] - df['salesquantity'].mean()) / (df['salesquantity'].std())
    df['price'] = (df['price'] - df['price'].mean()) / (df['price'].std())
    
    # uncomment this bunch of code below to preprocess dataset to create training/test data
    # train, test = load_dataset(num_items, df)
    # save_npz('input_training.npz', train[0])
    # np.save('label_training.npy', train[1])
    # save_npz('input_test.npz', test[0])
    # np.save('label_test.npy', test[1])
    print('Loading training/test data...')
    X_train = load_npz('input_training.npz')
    y_train = np.load('label_training.npy')
    X_test = load_npz('input_test.npz')
    y_test = np.load('label_test.npy')
    train = (X_train, y_train) 
    test = (X_test, y_test) 

    print('Training...')
    model = model(train[0], train[1], type_model=model_name)
    print('Evaluating...')
    y_pred = model.predict(test[0])
    precision, recall, fscore, support = precision_recall_fscore_support(test[1], y_pred, average='macro')
    print('Precision: ', precision)
    print('Recall: ', recall)
    print('F1-score: ', fscore)

    if model_name == 'RF' or model_name == 'gbc' or model_name == 'xgb':
        draw_feature_importance(num_items, model)


    



