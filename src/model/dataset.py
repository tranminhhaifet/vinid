import os
import json
import pandas as pd
import numpy as np
from scipy.sparse import csr_matrix, coo_matrix

def read_file(file_name):
    """
    Read the raw data file, return dataset as a dataframe
    """

    users = dict()
    user_id = 0

    items = dict()
    item_id = 0

    name_cols = ['user_id', 'date', 'month', 'item_id', 'salesquantity', 'price']
    list_trans = list()

    fp = open(file_name)
    for i, line in enumerate(fp):
        parser = line.strip().split(',')
        if i > 0:
            if parser[0] not in users.keys():
                users[parser[0]] = user_id
                user_id += 1
            timer = parser[1].split('-')
            date, month = int(timer[2]), int(timer[1])
            str_trans_info = ','.join(parser[2:])
            str_trans_info = str_trans_info.strip('"')

            str_trans_info = str_trans_info.replace('\'', '"')
            
            trans_info = json.loads(str_trans_info)
            
            for item_info in trans_info:
                if item_info['article'] not in items.keys():
                    items[item_info['article']] = item_id
                    item_id += 1
                record = [users[parser[0]], date, month, items[item_info['article']], item_info['salesquantity'], item_info['price']]
                list_trans.append(record)
    frame = pd.DataFrame(np.array(list_trans), columns=name_cols)
    return len(users), len(items), frame

def create_features(num_items, df, input_months, labeled_month):
    """
    Create feature vector X representing for each customer (user) and labels 
    Params:
        - num_items: total number of items in dataset
        - df: dataset represented as a dataframe
        - input_months: list of two months for creating feature vector X
        - labeled_month: the next month to assign label for each customer respectively
    """

    # number of days caculated from the beginning of first month in dataset
    # to caculate num_days_since_last_purchase feature
    days_from_first_months = {1: 0, 2: 28, 3: 59, 4: 89, 5: 120, 6: 150} 
    df_input = df.loc[df['month'].isin(input_months)] # select data in two months considered as input
    #print(df_input_train.describe())
    all_users = np.unique(df_input['user_id'].values) # array of users in two input months
    num_users = len(all_users)

    # create labels
    df_label = df.loc[df['month'] == labeled_month]
    df_label = df_label.loc[df_label['user_id'].isin(all_users)] # only assign label for customer in two input months
    id_users = np.array(np.unique(df_label['user_id'].values), dtype=np.int32)
    mapping = {all_users[i]: i for i in range(num_users)}
    labels = np.zeros(num_users, dtype=np.int32)
    for idu in id_users:
        labels[mapping[idu]] = 1

    # create matrix feature X
    last_month = input_months[-1] # the last month in two inputs months

    # three variables for creating a csr_matrix scipy.sparse
    data = list() 
    indptr = [0]
    indices = list()
    
    print(num_users, len(df_input.groupby('user_id')))
    for idu, df_user in df_input.groupby('user_id'):
        # create feature vector for each customer
        vector_quantity = np.zeros(num_items) # vector salesquantity of items
        vector_price = np.zeros(num_items)    # vector price of items
        for idi, df_user_item in df_user.groupby('item_id'):
            idi = int(idi)
            vector_quantity[idi] = sum(df_user_item['salesquantity'].values)
            vector_price[idi] = sum(df_user_item['price'].values) / vector_quantity[idi]
        vector_feature = np.append(vector_price, vector_quantity)
        # add feature number of unique items
        vector_feature = np.append(vector_feature, [len(np.unique(df_user['item_id'].values))])

        # more features
        groups = df_user.groupby(['date', 'month'])
        num_days = len(groups)
        list_days = list() # a list of days which customer make purchase
        size_of_orders = list() # a list of number of items per day
        price_of_orders = list() # a list of total price consumed per day
        for value, df_by_day in groups:
            list_days.append(value[0] + days_from_first_months[value[1]-1])
            size_of_orders.append(len(df_by_day))
            price_of_orders.append(sum(df_by_day['price'].values))
        
        list_days = np.sort(np.array(list_days))
        #size_of_orders = np.array(size_of_orders)
        # calculating period between two successive days
        if num_days > 1:
            d_days = list_days[1:] - list_days[:-1]
        else:
            d_days = np.array([0])
        # add new features to vetor_feature
        days_since_last_purchase = days_from_first_months[last_month] - list_days[-1]
        vector_feature = np.append(vector_feature, [num_days, np.mean(d_days), np.std(d_days), np.mean(size_of_orders), np.std(size_of_orders), days_since_last_purchase])
        vector_feature = np.append(vector_feature, [np.mean(price_of_orders), np.std(price_of_orders)])
        
        # for constructing sparse matrix features 
        id_nonzero = np.where(vector_feature > 0)[0]
        indices += id_nonzero.tolist()
        data += vector_feature[id_nonzero].tolist()
        indptr.append(len(indices))
    print(len(indptr), len(indices), len(data))
    matrix_features = csr_matrix((data, indices, indptr), shape=(num_users, len(vector_feature)))
    
    return matrix_features.tocoo(), labels


def load_dataset(num_items, df):
    print('Loading training data...')
    X_train1, y_train1 = create_features(num_items, df, [2,3], 4)
    size_X1 = X_train1.shape[0]
    num_features = X_train1.shape[1]
    X_train2, y_train2 = create_features(num_items, df, [3,4], 5)
    size_X2 = X_train2.shape[0]

    print('Loading test data...')
    X_test, y_test = create_features(num_items, df, [4,5], 6)
    size_t = X_test.shape[0]

    # concatenate all train/test data to make normalization
    row1, col1, data1 = X_train1.row, X_train1.col, X_train1.data
    row2, col2, data2 = X_train2.row, X_train2.col, X_train2.data
    row_t, col_t, data_t = X_test.row, X_test.col, X_test.data
    X = coo_matrix((np.concatenate((data1, data2, data_t)), (np.concatenate((row1, row2+size_X1, row_t+size_X1+size_X2)), np.concatenate((col1, col2, col_t)))), shape=(size_X1+size_X2+size_t, num_features))
    X = X.tolil()
    num_rows_X = X.shape[0]

    # Normalization
    print('Normalizing...')
    print(X.shape)
    for i in range(2*num_items, num_features):
        col = X[:, i].toarray()
        col = (col - np.mean(col)) / np.std(col) # standard normalization
        X[:, i] = col
    X = X.tocsr()
    X_train = X[:(size_X1 + size_X2)]
    X_test = X[(size_X1 + size_X2):]
    y_train = np.append(y_train1, y_train2)

    return (X_train, y_train), (X_test, y_test)


if __name__ == '__main__':
    num_users, num_items, df = read_file('MLTrack_DataSet.csv')
    
    train, test = load_dataset(num_items, df)
    print(train[0].shape, train[1].shape)
    print(test[0].shape, test[1].shape)
    print(len(np.where(train[1] == 0)[0]), len(np.where(train[1] == 1)[0]))
    print(len(np.where(test[1] == 0)[0]), len(np.where(test[1] == 1)[0]))