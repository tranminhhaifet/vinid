"""
date: 20/10/2018
@haitran
1. read dataset_csv to dataset_df
2. build feature vector for each customer
3. build labeled dataset (X,y)
4. model
"""

import numpy as np
from scipy import sparse
import pandas as pd
import matplotlib.pyplot as plt
import csv
import ast
import pickle


def read_dataset(filename):
    #filename = "MLTrack_DataSet.csv"
    #filename = "test.csv"
    with open(filename) as data_csv:
        dataset = csv.DictReader(data_csv)
        dataset_list = []

        record_count = 0
        print("read dataset_csv \n")
        for record in dataset:
            #print("record = ", record_count)
            record_count += 1

            csn = record['csn']
            date = record['date']
            purchased_list = ast.literal_eval(record['transaction_info'])

            for item in purchased_list:
                dataset_list.append([csn, date, item['article'], item['price'], item['salesquantity']])

        # write dataset_list to a dataframe
        dataset_df = pd.DataFrame(dataset_list, columns=['csn','date','item','price','salesquantity'])

        # normalize dataframe
        #dataset_df['price'] = (dataset_df['price'] -  dataset_df['price'].mean())/dataset_df['price'].std()
        #dataset_df['salesquantity'] = (dataset_df['salesquantity'] -  dataset_df['salesquantity'].mean())/dataset_df['salesquantity'].std()

        # write dataframe to csv
        dataset_df.to_csv('dataset_df.csv')

    return dataset_df


def cell_location(item_id, date):
    """
    convert: (item_id, date) to (row,col,month)
    """
    row = dict_item[item_id]
    col = np.int(date.split('-')[2])-1
    month = np.int(date.split('-')[1])

    return (row,col,month)


def build_custm_profile(dataset_df, moonths):
    """
    for each customer, build a vector of price/salesquantity aggregrated over 1/2 month
    """
    print("build customer profile and label \n")
    # extract dataset_df of customers presented in months [2,3]
    all_month = [date.split('-')[1] for date in dataset_df['date']]
    locations = [index for index, month in enumerate(all_month) if month in moonths[:2]]
    dataset_df_month = dataset_df.iloc[locations]

    # dict of customer in months[2,3]
    unique_csn = np.unique(list(dataset_df_month['csn']))
    dict_custm = dict(zip(unique_csn, range(len(unique_csn))))

    # dict of all item
    unique_item = np.unique(list(dataset_df['item']))
    dict_item   = dict(zip(unique_item, range(len(unique_item))))

    # number of customer and item
    nu_custm = len(dict_custm)
    nu_item  = len(dict_item)

    # customer feature matrix
    feature_matrix_price = sparse.lil_matrix((nu_custm,nu_item))
    feature_matrix_sale  = sparse.lil_matrix((nu_custm,nu_item))

    for csn, custm_id in dict_custm.items():
        print("customer id =", custm_id)
        csn_vector_price = np.zeros((1,nu_item))
        csn_vector_sale  = np.zeros((1,nu_item))
        csn_df = dataset_df_month.loc[dataset_df_month['csn']==csn]

        for index, record in csn_df.iterrows():
            (row,col,month) = cell_location(record['item'], record['date'])
            csn_vector_price[0,row] =  csn_vector_price[0,row] + np.float32(record['price'])
            csn_vector_sale[0,row]  =  csn_vector_sale[0,row] + np.float32(record['salesquantity'])

        # save csn_vector to feature_matrix
        feature_matrix_price[custm_id,:] = csn_vector_price[0,:]
        feature_matrix_sale[custm_id,:]  = csn_vector_sale[0,:]

    # dict of customer in month 4
    locations_month_next  = [index for index, month in enumerate(all_month) if month in moonths[2]]
    dataset_df_month_next = dataset_df.iloc[locations_month_next]
    unique_csn_month_next = np.unique(list(dataset_df_month_next['csn']))
    dict_custm_month_next = dict(zip(unique_csn_month_next, range(len(unique_csn_month_next))))

    # active customers who presented in month 2,3 and 4
    active_custm = set(dict_custm.keys()).intersection(set(dict_custm_month_next.keys()))

    # inactive cusotmers who presented in month 2, 3 and not 4
    inactive_custm = set(dict_custm.keys()).difference(set(dict_custm_month_next.keys()))

    # labeled customer
    y = np.zeros(len(dict_custm))
    for csn in dict_custm:
        if csn in active_custm:
            y[dict_custm[csn]] = 1
        if csn in inactive_custm:
            y[dict_custm[csn]] = 0

    return (dict_custm, active_custm, inactive_custm, feature_matrix_price, feature_matrix_sale, y)


# save processed data to disk using pickle
def save_data_to_disk(filename, dict_custm, active, inactive, X_price, X_salequantity, y):
    """
    save processed data to disk for later training and testing
    """
    with open(filename,'wb') as f:
        pickle.dump([dict_csn, active, inactive, X_price, X_salequantity, y],f)


# data visualization
def data_visualize():
    """
    """
    # read active/inactive set of csn
    with open('labeled_data_1.pkl','rb') as f:
        dict_csn, active_set, inactive_set, X_price, X_sale, y = pickle.load(f)

    # load dataframe
    df = pd.read_csv('dataset_df.csv')

    # df of active customer
    df_active = df.loc[df['csn'].isin(active_set)].copy()

    # df of inactive customer
    df_inactive = df.loc[df['csn'].isin(inactive_set)].copy()

    # map csn and item_id to index for plot
    unique_item = np.unique(list(df['item']))
    dict_item   = dict(zip(unique_item, range(len(unique_item))))

    unique_csn = np.unique(list(df['csn']))
    dict_csn   = dict(zip(unique_csn, range(len(unique_csn))))

    # scatter plot for active customer
    active_csn_index  = [dict_csn[csn_id] for csn_id in df_active['csn']]
    active_item_index = [dict_item[item_id] for item_id in df_active['item']]
    df_active['item_index']   = active_item_index
    df_active['csn_index']    = active_csn_index
    df_active.plot(kind='scatter',x='item_index',y='csn_index',alpha=0.6,
                  s=df_active['salesquantity'], label='salesquantity', c='price',cmap=plt.get_cmap("jet"),colorbar=True)
    plt.xlabel("item_index")
    plt.title("purchase of active customers")
    #plt.savefig("scatter1.png",dpi=1000)
    plt.show()

    # scatter plot for active customer
    inactive_csn_index  = [dict_csn[csn_id] for csn_id in df_inactive['csn']]
    inactive_item_index = [dict_item[item_id] for item_id in df_inactive['item']]
    df_inactive['item_index']   = inactive_item_index
    df_inactive['csn_index']    = inactive_csn_index
    df_inactive.plot(kind='scatter',x='item_index',y='csn_index',alpha=0.6,
                  s=df_inactive['salesquantity'], label='salesquantity', c='price',cmap=plt.get_cmap("jet"),colorbar=True)
    plt.xlabel("item_index")
    plt.title("purchase of inactive customers")
    #plt.savefig("scatter2.png",dpi=1000)
    plt.show()


    # histogram for active vs inactive customer
    df_active.hist(column='price', bins=100,color='DarkBlue')
    plt.xlabel('price')
    plt.ylabel('number of active customer')
    plt.title('active customer price historgram')
    plt.xlim(xmin = 0, xmax=1000000)
    #plt.savefig("hist1.png",dpi=1000)
    plt.show()

    df_inactive.hist(column='price', bins=100,color='Red')
    plt.xlabel('price')
    plt.ylabel('number of inactive customer')
    plt.title('inactive customer price historgram')
    plt.xlim(xmin = 0, xmax=1000000)
    #plt.savefig("hist2.png",dpi=1000)
    plt.show()

    # histogram for active vs inactive customer
    df_active.hist(column='salesquantity', bins=100,color='DarkBlue')
    plt.xlabel('price')
    plt.ylabel('number of active customer')
    plt.title('active customer salesquantity historgram')
    plt.xlim(xmin = 0, xmax=20)
    #plt.savefig("hist3.png",dpi=1000)
    plt.show()

    df_inactive.hist(column='salesquantity', bins=100,color='Red')
    plt.xlabel('salesquantity')
    plt.ylabel('number of inactive customer')
    plt.title('inactive customer salesquantity historgram')
    plt.xlim(xmin = 0, xmax=20)
    #plt.savefig("hist4.png",dpi=1000)
    plt.show()

    # histogram for active vs inactive customer
    df_active.hist(column='item_index', bins=100,color='DarkBlue')
    plt.xlabel('item_index')
    plt.ylabel('number of active customer')
    plt.title('active customer item_index historgram')
    #plt.savefig("hist5.png",dpi=1000)
    plt.show()

    df_inactive.hist(column='item_index', bins=100,color='Red')
    plt.xlabel('item_index')
    plt.ylabel('number of inactive customer')
    plt.title('inactive customer item_index historgram')
    #plt.savefig("hist6.png",dpi=1000)
    plt.show()

if __name__ == '__main__':

    #filename = "MLTrack_DataSet.csv"
    filename = "test.csv"
    dataset_df = read_dataset(filename)
    print("number of customer", len(np.unique(list(dataset_df['csn']))))
    print("number of item", len(np.unique(list(dataset_df['item']))))

    # dict customer and dict item
    unique_csn  = np.unique(list(dataset_df['csn']))
    unique_item = np.unique(list(dataset_df['item']))
    dict_csn    = dict(zip(unique_csn, range(len(unique_csn))))
    dict_item   = dict(zip(unique_item, range(len(unique_item))))

    # build labeled data set for month [2,3,4]
    dict_custm_1, active_1, inactive_1, X_price_1, X_sale_1, y_1 = build_custm_profile(dataset_df, ["02","03","04"])
    # build labeled data set for month [3,4,5]
    dict_custm_2, active_2, inactive_2, X_price_2, X_sale_2, y_2 = build_custm_profile(dataset_df, ["03","04","05"])
    print("number of active customer", np.sum(y_1==1))
    print("number of inactive customer", np.sum(y_1==0))

    # save labeled dataset to disk
    save_data_to_disk("labeled_data_1.pkl", dict_custm_1, active_1, inactive_1, X_price_1, X_sale_1, y_1)
    save_data_to_disk("labeled_data_2.pkl", dict_custm_2, active_2, inactive_2, X_price_2, X_sale_2, y_2)

    # data visualization
    data_visualize()
